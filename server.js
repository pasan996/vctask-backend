const express = require('express');
const bodyParser = require('body-parser');
const dbConfig =require('./config/db.config');
const mongoose =require('mongoose');
const User=require('./models/userModel')
const cors = require('cors')


const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.text());
app.use(cors());

mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// define a simple route
app.get('/', (req, res) => {
    res.send("hello world from server");
});

app.get('/users',paginate(User),(req,res) =>{
    res.json(res.paginate);
})


function paginate(model){
    return async (req,res,next)=>{
        console.log(req.query.page);
        const page=parseInt(req.query.page);
        const limit=parseInt(25);

        const results={}

        const startIndex=(page-1)*limit;
        const endIndex=page*limit;
        const total=parseInt(await model.countDocuments().exec());
        results.total=total
        console.log(page,endIndex,startIndex)
        if(endIndex <total ){
            results.next={
                page:page +1,
                limit:limit
            }
        }

        if(startIndex>0){
            results.previous={
                page:page-1,
                limit:limit
            }
        }

        results.result= await model.find().limit(limit).skip(startIndex).exec()
        res.paginate =results
        next();


    }
}



app.listen(3100, () => {
    console.log("Server is listening on port 3000");
});