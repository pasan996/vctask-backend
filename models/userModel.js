const mongoose = require('mongoose');

const User = mongoose.Schema({
    firstName: String,
    lastName: String,
    videoCount:Number
})

module.exports = mongoose.model('Users', User);